

## **Sensor**

- A sensor is a device that measures physical input from its environment and converts it into data that can be interpreted by either a human or a machine.
- It converts physical characteristics into electrical signals.


![Sensor](https://circuitdigest.com/sites/default/files/projectimage_tut/Basic-Sensors.jpg)


## **Actuator**

- It converts electrical signals into physical characteristics.i.e. reverse of sensors.
- An actuator is a mover/component of a machine that is responsible for moving and controlling a mechanism or system.
An actuator requires a control signal and a source of energy. 

 Example of actuators include :  Electric motors

 ![](https://www.rfwireless-world.com/images/Actuator.jpg)

 



---


## **Analog signal**

An analog signal is one type of continuous time-varying signal.A simple type of analog signal is nothing but a sine wave.

_Examples_ of analog signals are Human voice, Analog phones etc.

![Anaolog signal](https://techdifferences.com/wp-content/uploads/2016/08/Analog-Signal.jpg)


## **Digital signal**

  - A digital signal is a signal that is being used to represent data as a sequence of discrete values.
  - The signal can have two possible valid values; this is called a binary signal (0 or 1).

![Digital signal](https://lh3.googleusercontent.com/proxy/b8cpXmAFSG2Px-ru7it732XSC6C7cDr6ewp8cbarrwXWnfF4dk29TUa1sPvF9YUjGlrAEISpzS8tFBiW2wvlbT7oQYMcj2nBzuJkWCqLmPEmrIbHk8KgX9wmgQex49kW-JvOsxPz0fSGSvuysb7DFkfNkHA-_fNzlpy_2w)





### **Difference between Analog signal and Digital signal**


![](https://media.cheggcdn.com/study/870/8701e007-8704-4de7-8e43-88799d198442/10298-5.E-6LOC-i1.png)




**Example** :


![](https://electricalacademia.com/wp-content/uploads/2017/10/meter.gif)



---



## **Micro-processors Vs Micro-controllers**


| Sr. no. | Micro-processor | Micro-controller |
|--- |--- | --- |
| 1. | Microprocessor consists of only a Central Processing Unit | Micro Controller contains a CPU, Memory, I/O all integrated into one chip |
| 2. | Microprocessor is used in Personal Computers | Micro Controller is used in an embedded system |
| 3. | Microprocessor uses an external bus to interface to RAM, ROM, and other peripherals | Microcontroller uses an internal controlling bus |
| 4. | Microprocessors are based on Von Neumann model | Micro controllers are based on Harvard architecture |
| 5. | Microprocessor is complicated and expensive, with a large number of instructions to process | Microcontroller is inexpensive and straightforward with fewer instructions to process |
| 6. | Microprocessor has a smaller number of registers, so more operations are memory-based | Microcontroller has more register. Hence the programs are easier to write |
| 7. | It's used for general purpose applications that allow you to handle loads of data | It's used for application-specific systems |
| 8. | Applications : Calulators, accounting system, military applications etc. | Applications : Mobile phones, automobiles, washing machine, security alarm etc. |


---


## **Introduction of Raspberry Pi**

- Raspberry Pi is a small single board computer. By connecting peripherals like Keyboard, mouse, display to the Raspberry Pi, it will act as a mini personal computer. 
- Raspberry Pi is popularly used for real time Image/Video Processing, IoT based applications and Robotics applications.


Raspberry Pi is a

- Mini Computer
- Limited but large power for its size
- No storage
- It is a SOC (System On Chip)
- We can connect shields (Shields - addon functionalities)
- Can connect multiple Pi’s together
- Microprocessor
- Can load a linux OS on it
- Pi uses ARM
- Connect to sensors or actuators

![](https://sc02.alicdn.com/kf/H0dd8df99b5384f91b65ed4cbf9c8ef83U/239488783/H0dd8df99b5384f91b65ed4cbf9c8ef83U.jpg_.webp)

### **Raspberry Pi Interfaces :**

Interfaces are ways to connect a sensor to a microprocessor.

Raspberry Pi Interfaces :

1. GPIO
1. UART
1. SPI
1. I2C
1. PWM

---
 
  `GPIO (General Purpose Input/Output)`  :

   GPIO pins used to connect microcontrollers to other electronic devices like sensors, diodes, displays etc.


---

 `UART (Universal Asynchronous Receiver/Transmitter)`  :

  It is a physical circuit in a microcontroller use to transmit and receive serial data.

  ![](https://www.elprocus.com/wp-content/uploads/UART.jpg)

---

 `SPI (Serial Peripheral Interface)`  :

   - SPI is an interface bus commonly used to send data between microcontrollers and small peripherals such as sensors, SD cards. It uses separate clock and data lines, along with a select line to choose the device you wish to talk to.

  - The SPI bus specifies four logic signals  :
        
         SCLK: Serial Clock (output from master)
         MOSI: Master Out Slave In (data output from master)
         MISO: Master In Slave Out (data output from slave)
         SS: Slave Select ( often active low, output from master) 




   ![SPI](https://www.circuitbasics.com/wp-content/uploads/2016/01/Introduction-to-SPI-Master-and-Slave.png)

   
---
  
     `I2C (Inter-Integrated Circuit)`

  - I2C is a serial communication protocol, so data is transferred bit by bit along a single wire (the SDA line).
   - I2C Bus (Interface wires) consists of just two wires and are named as  _Serial Clock Line (SCL)_  and  _Serial Data Line (SDA)_. The data to be transferred is sent through the SDA wire and is synchronized with the clock signal from SCL.


![I2C](https://www.electronicshub.org/wp-content/uploads/2018/02/Basics-of-I2C-Communication-I2C-Bus.jpg)

---

`PWM (Pulse Width Modulation)  :`

 - PWM is a technique for getting analog results with digital means.
 - Included among the many applications for pulse-width modulation (PWM) are voltage regulation, power-level control, and fan-speed control.

![](https://prototyperobotics.com/system/images/3/large/pwm.jpg)


---

## **Serial Parallel communication :**


- Serial Transmission is the type of transmission in which a single communication link is used to transfer the data from an end to another. 
- On other hand Parallel Transmission is the transmission in which multiple parallel links are used that transmit each bit of data simultaneously. 


![](https://fab.academany.org/2019/labs/vigyanashram/students/jayadip-sarode/Assignment14/images/2.png)







