

# **IOT Protocols**


## **4-20mA**

- 4 to 20 MilliAmp A point-to-point or multi-drop circuit mainly used in the process automation field to transmit signals from instruments and sensors in the field to a controller. 
- It sends signal from 4 to 20 mA that represents 0 to 100% of some process variable.

### **4-20 mA Current Loops**

- It is an ideal method of transferring process information because current does not change as it travels from transmitter to receiver. 
- It is also much simpler and cost effective.
  
**Components of 4-20 mA current loops :**

![Components](https://www.predig.com/sites/default/files/images/Indicator/back_to_basics/4-20mA_Current_Loops/4-20mA_current_loop_components.jpg)


**1. Sensor**

- A `sensor` measures a physical quantity as temperature, humidity, flow, level or pressure. 

**2. Transmitter**

- `Transmitter` converts physical quantity measured by sensor into current signal between 4-20 mA. 

**3. Power Source**

- `Power` is the rate of doing work equivalent to an amount of energy consumed per unit time.
- The power source such as an electrical outlet, energy storage devices such as batteries or fuel cells, generators, solar power converters, or another power supply.
- The power supply must output a DC current.
- There are many common voltages that are used with 4-20 mA current loops 9V, 12V, 24V, etc.

**4. Loop**

- `Loop` refers to the actual wire connecting the sensor to the device receiving the 4-20 mA signal and then back to the transmitter.
- The current signal on the loop is regulated by the transmitter according to the sensor's measurement.

**5. Receiver**

- `Receiver` is a device which can receive and interpret the current signal and display the information received for monitoring purpose.
- Digital displays, controllers, actuators, and valves are common devices to incorporate into the loop.
  



**Featutres of 4-20 mA Loops**

- The 4-20 mA current loop is the domain standard in many industries.
- It is the simplest option to connect and configure.
- It uses less wiring and connections than other signals, greatly reducing initial setup costs.
- Better for traveling long distances, as current does not degrade over long connections like voltage.
- It is less sensitive to background electrical noise.


---


### **MODBUS Communication Protocol**

- `Modbus` is a communication protocol used for transmitting information over serial lines between electronic devices.
- In a standard Modbus network, there is one Master and up to 247 Slaves, each with a unique Slave Address from 1 to 247.
- Modbus is used in multiple client-server applications to monitor and program devices, to communicate between intelligent devices and sensors and instruments.
  

![](https://theautomization.com/wp-content/uploads/2017/10/Modbus-Plant.png)


- Modbus protocol can be used over 2 interfaces

     - RS485 - called as Modbus RTU
    - Ethernet - called as Modbus TCP/IP


#### **RS 485 - MODBUS RTU**

- RS485 is a serial data transmission standard widely used in industrial implementations.
- The MODBUS RS485 protocol defines communication between a host (master) and devices (slaves) that allows querying of device configuration and monitoring. 

![RS 485](https://www.controlglobal.com/assets/Uploads/1812-Feat3-Fig1-650-compressor.png)


#### **Ethernet - MODBUS TCP/IP**

- Modbus TCP/IP combines a physical network (Ethernet), with a networking standard (TCP/IP).
- Ethernet Basics TCP/IP (Transmission Control Protocol/Internet Protocol) is a set of protocols independent of the physical medium used to transmit data, but most data transmission for Internet communication begins and ends with Ethernet frames.

![](https://www.fleaplc.it/images/Articoli/S7OpenModbus/modbus-graph-en-zoom.jpg)


---


### **OPC UA Protocol**

- OPC Unified Architecture (OPC UA) is a machine to machine communication protocol for industrial automation, based on a client server communication.


![OPCUA](https://gitlab.com/iotiotdotin/project-internship/iotmodule2/-/wikis/uploads/5919e2325a45423a158c276511dde466/opcua1.png)


- OPC UA is a modern communications protocol for industrial automation that is increasingly being adopted for data collection and control by traditional on-premises applications and Industrial IoT and Industry 4.0 applications and platforms.


---

`OPC UA Client - Server  :` 


  

![](https://www.novotek.com/images/solutionpages/Kepware_solutionpages/2015_OPCmatrix.png)


---


## **Cloud Protocol**

---

### **MQTT (Message Queuing Telemetry Transport)**

- MQTT is lightweight, **publish-subscribe** network protocol that transports messages between devices.
- MQTT Client as a publisher sends a message to the **MQTT broker** whose work is to distribute the message accordingly to all other MQTT clients subscribed to the topic on which publisher publishes the message.
- **MQTT topics** are a form of addressing that allows MQTT clients to share information (publish the messages).


![MQTT](https://iotraspi2.files.wordpress.com/2016/02/pub-sub-mqtt-1024x588.png?w=700)


---



### **HTTP (Hyper Text Transfer Protocol)**

- HTTP is a TCP/IP based communication protocol, that is used to deliver data on the World Wide Web. The default port is TCP 80, but other ports can be used as well.
- HTTP based on `request-response`. Client send request to the server, and the servers respond to these requests.
- Here web browsers, robots and search engines, etc. act like HTTP clients, and the Web server acts as a server.


### **The Request**

The request has 3 parts

- Request line
- HTTP header
- message body

GET request is a type of HTTP request using the GET method.

 There are different types of methods

| Sr.no. | Method | Description |
|---|---|---|
| 1. | GET | The GET method is used to retrieve information from the given server using a given URI.  |
| 2. | HEAD | Same as GET, but it transfers the status line and the header section only. |
| 3. | Post | A POST request is used to send data to the server, for example, customer information, file upload, etc. |
| 4. | PUT | Replaces all the current representations of the target resource with the uploaded content. |
| 5. | DELETE | Removes all the current representations of the target resource given by URI. |
| 6. | CONNECT |  Establishes a tunnel to the server identified by a given URI. |
| 7. | OPTIONS | Describe the communication options for the target resource. |
| 8. | TRACE | TRACE allows the client to see what is being received at the other end of the request chain and use that data for testing or diagnostic information |


### **The Response**

The Response is made of 3 parts

- Status line
- HTTP header
- Message body

![status code](https://gitlab.com/iotiotdotin/project-internship/iotmodule2/-/wikis/uploads/1dcbcc2c7e58e59a042e55b2b2080405/restful-web-services-with-spring-mvc-28-638.jpg)



---







